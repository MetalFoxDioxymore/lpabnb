module.exports = {
    content: [
        "./storage/framework/views/*.php",
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.vue",
    ],
  theme: {
    extend: {
        colors: {
            'primary-light': '#CBD5E1',
            primary: '#475569',
            'primary-dark': '#0F172A',
            secondary: '#1a202c',
            'secondary-light': '#4a5568',
           danger: '#913333',
        }
    },
  },
  plugins: [],
}
