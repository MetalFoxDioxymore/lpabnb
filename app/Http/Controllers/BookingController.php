<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBookingRequest;
use App\Http\Requests\UpdateBookingRequest;
use App\Models\Booking;
use App\Models\Customer;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Booking/Index', [
            'bookings' => Booking::query()->orderBy('starts_at', 'ASC')->with('customers')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Booking/Create', [
            'bookings' => Booking::query()->with('customers')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBookingRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreBookingRequest $request)
    {
        $bookingData = $request->validated();
        $customersId = [];

        foreach ($bookingData['customers'] as $customerData) {
            if ($customerData['id'] === null) {
                $customer = Customer::create($customerData);
                $customersId[$customer->id] = [
                    'is_main' => false,
                ];
            }
            else
            {
                $customersId[$customerData['id']] = [
                    'is_main' => false,
                ];
            }
        }

        $booking = Booking::create($bookingData);
        $booking->customers()->sync($customersId);

        return Redirect::route('bookings.show', $booking);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        return Inertia::render('Booking/Show', [
            'booking' => $booking->load('customers'),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        return Inertia::render('Booking/Edit', [
            'bookings' => Booking::query()->with('customers')->get(),
            'booking' => $booking->load('customers'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBookingRequest  $request
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBookingRequest $request, Booking $booking)
    {
        $bookingData = $request->validated();
        $customersId = [];

        foreach ($bookingData['customers'] as $customerData) {
            if ($customerData['id'] === null) {
                $customer = Customer::create($customerData);
                $customersId[$customer->id] = [
                    'is_main' => false,
                ];
            }
            else
            {
                $customersId[$customerData['id']] = [
                    'is_main' => false,
                ];
            }
        }

        $booking->fill($bookingData);
        $booking->save($bookingData);
        $booking->customers()->sync($customersId);

        return Redirect::route('bookings.show', $booking);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();

        return Redirect::route('bookings.index');
    }
}
