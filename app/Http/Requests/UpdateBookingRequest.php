<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBookingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'starts_at' => 'date',
            'ends_at' => 'date',
            'babies_count' => 'integer',
            'pets_count' => 'integer',
            'notes' => 'string|nullable',
            'customers' => 'array|required',
            'customers.*.first_name' => 'string|required',
            'customers.*.last_name' => 'string|required',
        ];
    }
}
