<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * @property bool $is_main
 * @property int $booking_id
 * @property int $customer_id
 */
class BookingCustomer extends Pivot
{
    //
}
