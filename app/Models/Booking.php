<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property Carbon $starts_at
 * @property Carbon $ends_at
 * @property integer $babies_count
 * @property integer $pets_count
 */
class Booking extends Model
{
    use HasFactory, HasTimestamps;

    protected $guarded = [
        'customers'
    ];

    public function customers()
    {
        return $this->belongsToMany(Customer::class)->using(BookingCustomer::class);
    }
}
