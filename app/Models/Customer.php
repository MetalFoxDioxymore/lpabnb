<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

/**
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $address
 */
class Customer extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function bookings()
    {
        return $this->belongsToMany(Booking::class)->using(BookingCustomer::class);
    }
}
