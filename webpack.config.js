const path = require("path");

module.exports = {
    resolve: {
        modules: [
            "node_modules",
        ],
        alias: {
            "@": path.resolve("resources/js"),
        },
    },
    stats: { children: true },
};
